#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tests pip wrapper.

Pip Return Codes:

    * 0 == Success
    * 1 == Package not found
    * 2 == Insufficient Permissions

"""
import pip
import pytest
import importlib

import hy

import hyem.packagers.hypip
from hyem.packagers.hypip import pip_supported
from hyem.packagers.hypip import pip_install
from hyem.packagers.hypip import pip_upgrade
from hyem.packagers.hypip import pip_init
from hyem.packagers.hypip import pip_install_import

TEST_PACKAGE = "pysnipp"
BAD_PACKAGE_NAME = "abc123zzz"


@pytest.yield_fixture(autouse=True)
def run_around_tests():
    """This fixture will be run code before and after every test.

    """
    # Code that will run before test
    pip.main(["uninstall", "-y", TEST_PACKAGE])

    # A test function will be run at this point
    yield

    # Code that will run after test
    pip.main(["uninstall", "-y", TEST_PACKAGE])


# Test pip supported ###################################################


def test_pip_supported():
    """Test pip is detected.

    """
    pip_supported()


# Test base pip install #############################################


def test_pip_install_success():
    """Test pip install of a package.

    pip returns ``0`` on successful installation, or if the requirement is
    already satisfied.

    """
    # Verify not installed
    with pytest.raises(ImportError, message="Expecting ImportError"):
        import pysnipp

    assert pip_install(TEST_PACKAGE) == 0
    assert pip_install(TEST_PACKAGE) == 0  # Requirement already satisfied


def test_pip_install_failure_name():
    """Test pip error code given a bad package name.

    pip returns ``1`` when a package is not found.

    """
    # Verify not installed
    with pytest.raises(ImportError, message="Expecting ImportError"):
        import pysnipp

    assert pip_install(BAD_PACKAGE_NAME) == 1


def test_pip_install_args():
    """Test pip error code given a bad package name.

    pip returns ``2`` when a package is not found.

    """
    # Verify not installed
    with pytest.raises(ImportError, message="Expecting ImportError"):
        import pysnipp

    assert pip_install(TEST_PACKAGE, args=["--quiet"]) == 0


def test_pip_install_failure_permissions():
    """Test pip error code root install without permissions.

    pip returns ``2`` when a package is not found.

    """
    # Verify not installed
    with pytest.raises(ImportError, message="Expecting ImportError"):
        import pysnipp

    assert pip_install(TEST_PACKAGE, root=True) == 2


def test_pip_install_failure_permissions_args():
    """Test pip error code root install without permmissions and args.

    pip returns ``2`` when a package is not found.

    """
    # Verify not installed
    with pytest.raises(ImportError, message="Expecting ImportError"):
        import pysnipp

    assert pip_install(TEST_PACKAGE, root=True, args=["-v"]) == 2



# Test pip upgrade #####################################################


def test_pip_upgrade():
    """Test pip upgrade of a package.

    """
    assert pip_upgrade(TEST_PACKAGE) == 0


# Test pip init #####################################################


def test_pip_install_success():
    """Test pip upgrade of self.

    """
    assert pip_init() == 0


# Test pip install import ##############################################


def test_pip_install_import():
    """Test pip install then import

    """
    # Verify not installed
    with pytest.raises(ImportError, message="Expecting ImportError"):
        import pysnipp

    pip_install_import(TEST_PACKAGE, args=["--ignore-installed"])

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tests recipes utilities.

"""
import pytest

import hy

from hyem.utils.hyaml import is_multi_level
from hyem.utils.hyaml import is_contains_key
from hyem.utils.hyaml import is_contains_value

@pytest.yield_fixture(autouse=True)
def run_around_tests():
    """This fixture will be run code before and after every test.

    """
    # Code that will run before test
    #pip.main(["uninstall", "-y", TEST_PACKAGE])

    # A test function will be run at this point
    yield

    # Code that will run after test
    #pip.main(["uninstall", "-y", TEST_PACKAGE])

def test_multi_level():
    """Test detection of multi-level data structures.

    """
    assert is_multi_level([[]])
    assert is_multi_level([{}])
    assert is_multi_level({1: {}})
    assert is_multi_level({'one': []})
    assert is_multi_level({'one': {'two': 'three'}})
    assert is_multi_level([{'one': 'two'}])
    assert is_multi_level([{'one': 1}, {'two': 2}])
    assert is_multi_level([{x[0]: x[1]} for x in
                           { x: x**2 for x in range(100000)}.items()])
    assert not is_multi_level([])
    assert not is_multi_level({})
    assert not is_multi_level({'foo': 'bar'})
    assert not is_multi_level({'foo': 'bar', 'spam': 'eggs'})
    assert not is_multi_level(['foo', 'bar'])
    assert not is_multi_level([x for x in range(100000)])
    assert not is_multi_level({ x: x**2 for x in range(100000)})

def test_dict_contains_key():
    """Test dict-contains-key with simplest case.

    """
    test_dict = {'foo': 'bar'}
    assert is_contains_key('foo', test_dict)
    assert not is_contains_key('bar', test_dict)
    assert not is_contains_key('baz', test_dict)

def test_contains_key_multple_keys():
    """Test dict-contains-key with multiple key dictionary.

    """
    test_dict = {'foo': 'bar', 'spam': 'eggs', 1: 'one', 'two': 2}
    assert is_contains_key('foo', test_dict)
    assert is_contains_key('spam', test_dict)
    assert is_contains_key(1, test_dict)
    assert is_contains_key('two', test_dict)
    assert not is_contains_key('one', test_dict)
    assert not is_contains_key('bar', test_dict)
    assert not is_contains_key('eggs', test_dict)
    assert not is__contains_key(2, test_dict)
    assert not is_dict_contains_key('baz', test_dict)

# contains_key() ###################################################

def test_contains_key_simple():
    """Test contains-key with simplest case.

    """
    assert is_contains_key('foo', {'foo': 'bar'})
    assert is_contains_key('two', {'two': 'three'})
    assert is_contains_key('one', {'one': 1})
    assert is_contains_key('two', {'two': 2})
    assert not is_contains_key('bar', {'foo': 'bar'})
    assert not is_contains_key('baz', {'foo': 'bar'})

def test_contains_key_multple_keys():
    """Test contains-key with multiple key dictionary.

    """
    test_dict = {'foo': 'bar', 'spam': 'eggs', 1: 'one', 'two': 2}
    assert is_contains_key('foo', test_dict)
    assert is_contains_key('spam', test_dict)
    assert is_contains_key(1, test_dict)
    assert is_contains_key('two', test_dict)
    assert not is_contains_key('one', test_dict)
    assert not is_contains_key('bar', test_dict)
    assert not is_contains_key('eggs', test_dict)
    assert not is_contains_key(2, test_dict)
    assert not is_contains_key('baz', test_dict)

def test_contains_key_multple_dict_levels():
    """Test contains-key with multiple level dictionary.

    """
    test_dict = {'one': {'two': 'three'}}
    assert is_contains_key('one', test_dict)
    assert is_contains_key('two', test_dict)
    assert not is_contains_key('one', {'two': 'three'})

def test_contains_key_multple_list_levels():
    """Test contains-key with list containing dictionaries.

    """
    test_dict = [{'one': 1}, {'two': 2}]
    assert is_contains_key('one', test_dict)
    assert is_contains_key('two', test_dict)
    assert not is_contains_key('three', test_dict)

def test_contains_value_simple():
    """Test contains-value with simplest case.

    """
    assert is_contains_value('foo', 'bar', {'foo': 'bar'})
    assert is_contains_value('two', 'three', {'two': 'three'})
    assert is_contains_value('one', 1, {'one': 1})
    assert is_contains_value('two', 2, {'two': 2})
    assert not is_contains_value('foo', 'baz', {'foo': 'bar'})
    assert not is_contains_value('bar', 'foo', {'foo': 'bar'})
    assert not is_contains_value('bar', 'baz', {'foo': 'bar'})
    assert not is_contains_value('baz', 'bar', {'foo': 'bar'})

def test_contains_value_multple_keys():
    """Test contains-value with multiple key dictionary.

    """
    test_dict = {'foo': 'bar', 'spam': 'eggs', 1: 'one', 'two': 2}
    assert is_contains_value('foo', 'bar', test_dict)
    assert is_contains_value('spam', 'eggs', test_dict)
    assert is_contains_value(1, 'one', test_dict)
    assert is_contains_value('two', 2, test_dict)
    assert not is_contains_value('one', 'bar', test_dict)
    assert not is_contains_value('bar', 'foo', test_dict)
    assert not is_contains_value('eggs', 2, test_dict)
    assert not is_contains_value(2, 1, test_dict)
    assert not is_contains_value('baz', 1, test_dict)

def test_contains_value_multple_dict_levels():
    """Test contains-value with multiple level dictionary.

    """
    test_dict = {'one': {'two': 'three'}}
    assert is_contains_value('two', 'three', test_dict)
    assert is_contains_value('one', {'two': 'three'}, test_dict)
    assert not is_contains_value('two', 'one', test_dict)
    assert not is_contains_value('one', 'three', test_dict)
def test_contains_value_multple_list_levels():
    """Test contains-value with list containing dictionaries.

    """
    test_dict = [{'one': 1}, {'two': 2}]
    assert is_contains_value('one', 1, test_dict)
    assert is_contains_value('two', 2, test_dict)
    assert not is_contains_value('one', 2, test_dict)
    assert not is_contains_value('two', 1, test_dict)
    assert not is_contains_value(1, 'one', test_dict)
    assert not is_contains_value(2, 'two', test_dict)
    assert not is_contains_value('three', 3, test_dict)
    assert not is_contains_value(3, 'three', test_dict)
    assert not is_contains_value(2, 1, test_dict)


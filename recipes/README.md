# Recipes

The recipes follow the [YAML](https://en.wikipedia.org/wiki/YAML) format to be both human and machine readable. Each file is a recipe for installation of a single package, along with metadata to aid in the decision if it should be installed, and also dependencies for handling the installation order.

## Metadata

The package name defaults to the pip name, and then on Debian `apt` if not on PyPi. To determine if privileges are needed the `sudo` key holds a boolean value.

### Pakagers

Each available packager is available as a key under the `packagers` key, the value is the install name for the package.

*   pip
*   gem
*   apt
*   pacman
*   rpm
*   portage
*   port

### Source

Source may be either a git repository or an archive.

### Categories

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-yw4l">baseline</th>
    <th class="tg-yw4l">essential system tools<br></th>
  </tr>
  <tr>
    <td class="tg-yw4l">development</td>
    <td class="tg-yw4l">development libs and tools<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">flair</td>
    <td class="tg-yw4l">useful but showy<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">language</td>
    <td class="tg-yw4l">programming languages<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">media</td>
    <td class="tg-yw4l">video, photo, editing<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">security</td>
    <td class="tg-yw4l">blue team and red team<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">tools</td>
    <td class="tg-yw4l">system admin tools<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">docs</td>
    <td class="tg-yw4l">for LaTeX, writing, etc.<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">lib</td>
    <td class="tg-yw4l">system software libraries</td>
  </tr>
  <tr>
    <td class="tg-yw4l">fun</td>
    <td class="tg-yw4l">neat but not useful<br></td>
  </tr>
</table>

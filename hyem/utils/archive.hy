#! /usr/bin/env hy

(import os)
(import [os.path :as op])
(import zipfile)
(import tarfile)
(import tempfile)
(import shutil)

(defn tarfile? [filename]
  "Return True if file is a tarfile False otherwise"
  (tarfile.is-tarfile filename))

(defn zipfile? [filename]
  "Return True if file is a zipfile False otherwise"
  (zipfile.is-zipfile filename))

(defn archive? [filename]
  "Return True if file is an archive False otherwise"
  (and (op.isfile path)
    (or (tarfile? filename) (zipfile? filename))))

(defn extract-to [filename path]
  "Extract files in an archive to a temp directory"
  (cond
    [tarfile? (setv archive (tarfile.open filename))]
    [zipfile? (setv archive (zipfile.open filename))]
    [True (raise (RuntimeError "[*] Cannot extract files unknown archive type"))])
  (cond
    [(= (len (.getnames archive)) 1)
      (setv prefix (first (op.split (first (.getnames archive)))))]
    [True
      (setv prefix (op.commonprefix (.getnames archive)))])
  (cond 
    [(= prefix ".")
      (if-not (op.isdir path) (os.mkdir path))
      (.extractall archive :path path)]
    [True
      (setv tmp-dir (op.normpath (tempfile.mkdtemp)))
      (.extractall archive :path tmp-dir)
      (shutil.move (op.normpath (op.join tmp-dir prefix)) path)])
  (.close archive))

;;;; hyaml.hy

(import yaml)
(import [functools [partial]])

;;; Parsing into data structures.

(defn yaml-to-dict [path]
  "Parse YAML at path into a dictionary"
  (with [f (open path)]
    (yaml.load f)))

(defn yaml-dict-list [lst]
  "Create a list of dictionaries from list of YAML file paths"
  (if-not (first lst)
    []
    (+ [(yaml-to-dict (first lst))] (yaml-dict-list (list (rest lst))))))

;;; Querying resulting data structures contents.

(defn multi-level? [field]
  "Return true if a list or dictionary contains another list or dictionary"
  (setv list-or-dict (fn [item] (or (instance? list item) (instance? dict item))))
  (if (instance? list field)
      (any (map coll? field))
      (if (instance? dict field)
          (any (map coll? (.values field))) False)))

(defn contains-key? [key field]
  "Return true if a data structure contains key else false.
   Breadth first search ends on first occurance."
  (if (instance? dict field)
      (if (in key (.keys field)) True
          (if (multi-level? field)
              (any (map (partial contains-key? key) (.values field))) False))
      (if (instance? list field)
          (if (multi-level? field)
              (any (map (partial contains-key? key) field)) False))))

(defn contains-value? [key value field]
  "Return true dict contains key-value pair else false"
  (if (instance? dict field)
      (if (in key (.keys field))
          (= value (get field key))
          (if (multi-level? field)
              (any (map (partial contains-value? key value) (.values field))) False))
      (if (instance? list field)
          (if (multi-level? field)
              (any (map (partial contains-value? key value) field)) False))))

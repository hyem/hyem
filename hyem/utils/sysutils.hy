(import os)
(import [os.path :as op])
(import [glob [glob]])

(defn path-list []
  "Get environment path as list"
  (.split (os.environ.get "PATH" "") os.pathsep))

(defn on-path [filename]
  "Build list of all possible places on PATH a file could be"
  (setv paths (path-list))
  (list-comp (op.join path filename) [path paths]))

(defn command? [program]
  "Check if command exists and is executable"
  (setv exe? (fn [path] (and (op.isfile path) (os.access path os.X_OK))))
  (if (and (first (op.split program )) (exe? program))
      program
      (list (filter exe? (on-path program)))))

(defn touch [path]
  "Create path and file if it does not exist"
  (setv basedir (op.dirname path))
  (if (not (op.exists basedir))
      (os.makedirs basedir))
  (with [f (open path "a")]
    (os.utime path None)))

(defn lines [path]
  "Get lines of a file as a list"
  (with [f (open path "r")]
    (list-comp (.strip line) (line (.readlines f)))))

(defn files [path]
  "Get all files in a directory as a list"
  (glob path))

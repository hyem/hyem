#!/bin/python env
""" install.py

Install prefered packages, and configure the system.

"""

import os
from glob import glob
from subprocess import call

import requests


INFILES = ['development.txt', 'security.txt', 'language.txt',
           'tools.txt', 'docs.txt', 'fun.txt', 'media.txt']
REALNAME = "Derek Goddeau"
EMAIL = "datenstrom@protonmail.com"
EDITOR = "vim"


def setup():
    """ Main function, run all setup functions.

    """
    banner("apt-get")
    install()       # Install system packages
    banner("pip")
    pip()           # Install pip packages
    banner("system config")
    system()        # Configure system settings
    banner("git config")
    git_config()    # Configure git settings
    banner("compile vim")
    vim()           # Compile vim from source
    banner("configure vim")
    vim_plugins()   # Configure vim
    banner("fonts")
    font()          # Install losevka font
    banner("neofetch")
    neofetch()      # Install neofetch for shell banner
    banner("oh-my-zsh")
    oh_my_zsh()     # Install the `Oh My Zsh` plugin manager for zsh shell
    banner("default shell")
    choose_shell()  # Ask if zsh or bash should be default


def install():
    """ Install Debian system packages.

    """
    packages = my_packages()
    success = call(["sudo", "apt-get", "update"])
    for package in packages:

        if '#' in package:
            continue

        success = call(["sudo", "apt-get", "install", "-y", package])
        if success != 0:
            print("[*] Installation failed for: {}".format(package))


def my_packages():
    """ Gather all system packages for Debian systems.

    """
    packages = []
    for file in INFILES:
        with open('package_lists/' + file, 'r') as infile:
            packages += [line.strip() for line in infile]
    return packages


def pip():
    """ Install pip packages

    """
    packages = pip_packages()
    for package in packages:

        if '#' in package:
            continue

        call(["pip3", "install", "--user", "--ignore-installed", package])


def pip_packages():
    """ Gather pip packages for install

    Pip does not handle the correct install order of dependancies when
    using ``pip install -r mypackages.txt``. By gathering them into a
    list they can be installed in the correct order.

    """
    packages = []
    with open('package_lists/pip.txt', 'r') as infile:
        packages += [line.strip() for line in infile]
    return packages


def system():
    """ Change system defaults.

    """
    call(["sudo", "update-alternatives", "--set", "editor",
          "/usr/bin/vim.basic"])


def git_config():
    """ Change git configuration.

    """
    call(["git", "config", "--global", "user.name", REALNAME])
    call(["git", "config", "--global", "user.email", EMAIL])
    call(["git", "config", "--global", "core.editor", EDITOR])


def vim():
    """ Install vim from source.

    Note:
        Python config directory may need to be changed for different distros.

    * Python 3 Support
    * Ruby Support
    * Lua Support

    """
    call(["bash install_vim.sh"], shell=True, cwd='scripts')


def vim_plugins():
    """ Install support for vim plugins.

    """
    home = os.path.expanduser('~/')
    vim_doc = home + '.vim/doc'
    vim_colors = home + '.vim/colors'
    vim_bitmaps = home + '.vim/bitmaps'
    vim_plugin = home + '.vim/plugin'
    vim_autoload = home + '.vim/autoload'

    call(["mkdir", "-p", vim_doc])
    call(["mkdir", "-p", vim_plugin])
    call(["mkdir", "-p", vim_colors])
    call(["mkdir", "-p", vim_bitmaps])
    call(["mkdir", "-p", vim_autoload])

    # Plugin manager
    repo = "https://github.com/junegunn/vim-plug.git"
    call(["git", "clone", repo])
    call(["cp", "-rf", "vim-plug/plug.vim", vim_autoload])
    call(["rm", "-rf", "vim-plug"])

    # Powerline fonts for statusbar
    repo = "https://github.com/powerline/fonts.git"
    call(["git", "clone", repo])
    call(["bash", "fonts/install.sh"])
    call(["rm", "-rf", "fonts"])

    # Solarized Color
    repo = "https://github.com/altercation/vim-colors-solarized.git"
    call(["git", "clone", repo])

    docs = glob("vim-colors-solarized/doc/*")
    colors = glob("vim-colors-solarized/colors/*")
    bitmaps = glob("vim-colors-solarized/bitmaps/*")
    autoloads = glob("vim-colors-solarized/autoload/*")

    for file in docs:
        call(["mv", "-f", file, vim_doc])
    for file in colors:
        call(["mv", "-f", file, vim_colors])
    for file in bitmaps:
        call(["mv", "-f", file, vim_bitmaps])
    for file in autoloads:
        call(["mv", "-f", file, vim_autoload])

    call(["rm", "-rf", "vim-colors-solarized"])


def font():
    """ Install losevka font.

    Releases page: https://github.com/be5invis/Iosevka/releases

    Patched NerdFont: https://github.com/ryanoasis/nerd-fonts

    """
    home = os.path.expanduser('~/')
    font_dir = home + ".fonts"

    call(["mkdir", "-p", font_dir])

    fonts = ("https://github.com/be5invis/Iosevka/releases/"
             "download/v1.12.3/01-iosevka-1.12.3.zip")
    call(["wget", fonts])
    call(["unzip", "01-iosevka-1.12.3.zip"])
    call(["rm", "-rf", "01-iosevka-1.12.3.zip"])

    font_files = glob("*.ttf")
    for font_file in font_files:
        call(["mv", "-f", font_file, font_dir])
        call(["rm", "-f", font_file])
    call(["fc-cache"])

    # Fetch patched Nerd Font version
    url = ("https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts" +
           "/Iosevka/Medium/complete/Iosevka%20Medium%20Nerd%20Font" +
           "%20Complete%20Mono.ttf")
    print(url)

    with open(font_dir + '/Iosevka-Medium-Patched-Mono.ttf', 'wb') as handle:
        response = requests.get(url, stream=True)

        if not response.ok:
            print("[*] Error Downloading Patched Font")
        for block in response.iter_content(1024):
            handle.write(block)


def neofetch():
    """ Install neofetch program

    """
    call(["bash install_neofetch.sh"], shell=True, cwd='scripts')


def oh_my_zsh():
    """ Install Oh My Zsh

    """
    home = os.path.expanduser('~/')

    # Theme for Oh My Zsh
    call(["mkdir", "-p", home + '.oh-my-zsh/custom/themes/'])
    powerlevel9k = ['git',
                    'clone',
                    'https://github.com/bhilburn/powerlevel9k.git']
    call(powerlevel9k, cwd=home + '.oh-my-zsh/custom/themes/')

    # Oh My Zsh
    command = ('sh -c "$(curl -fsSL https://raw.githubusercontent.com/' +
               'robbyrussell/oh-my-zsh/master/tools/install.sh)"')
    call([command], shell=True)

    # Restore the real .zshrc
    home = os.path.expanduser('~/')
    backup = home + '.zshrc.pre-oh-my-zsh'
    zshrc = home + '.zshrc'
    call(['cp', backup, zshrc])


def choose_shell():
    """ Choose which default shell to use.

    """
    print("[*] Oh My Zsh has changed the default shell from bash to zsh.")
    print("[*] A zsh shell launcher has been installed for access along bash.")
    answer = None
    while answer not in ['y', 'n', '']:
        answer = input("[*] Revert back to bash shell default? (Y/n)")
        answer = answer.lower()

    success = False
    if answer in ['y', '']:
        success = call(['sudo chsh --shell=/bin/bash $USER'], shell=True)

    if success != 0:
        print("[*] Warning: could not set default shell back to bash")
        print("[*] Info: run `sudo chsh --shell=/bin/bash $USER` as root")


def banner(string):
    """ Given a string print a banner with the string centered.

    """
    print('-' * 72)
    print(string.center(72, ' '))
    print('-' * 72)

if __name__ == '__main__':
    setup()

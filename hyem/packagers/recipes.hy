;;;; recipes.hy
;;;;
;;;; Utilities for interacting with the recipe data structures. 
;;;;

(import [hyem.utils.hyaml [contains-key? contains-value?]])

;;; Attribute queries. 

(defn pkg-attribute? [recipe attribute]
  "Return true if attribute (key) exists anywhere in recipe"
  (contains-key? attribute recipe))

(defn pip? [recipe]
  "Return true if pip install is available for package"
  (if (in "packagers" recipe)
      (in "pip" (get recipe "packagers"))))

(defn deb? [recipe]
  "Return true if apt install is available for package"
  (if (in "packagers" recipe)
      (in "deb" (get recipe "packagers"))))

(defn sudo? [recipe]
  "Return true if sudo is needed for installation"
  (contains-value? "sudo" True recipe))

;;; Filter recipes by package attributes.

(defn content-filter [dict-list attribute? &optional [inverse None]]
  "Given a list of dictionaries filter then using the supplied function"
  (list (filter 
        (if inverse 
          (fn [d] (not (attribute? d)))
          (fn [d] (attribute? d)))
        dict-list)))

;;; Get package install list by attributes.

(defn pip-packages [recipes]
  "Return a list of all pip packages"
  (setv acc [])
  (for (recipe (content-filter recipes pip?))
       (.append acc (get (get recipe "packagers") "pip"))) acc)

(defn apt-packages [recipes]
  "Return a list of all pip packages"
  (setv acc [])
  (for (recipe (content-filter recipes deb?))
       (.append acc (get (get recipe "packagers") "deb"))) acc)

#! /usr/bin/env hy
(import sh)
(import shutil)
(import [sh [apt-get]])
(import [hyem.packagers.hypip [pip-install-import]])

(defn apt-supported []
  "Determine if apt(-get) is supported"
  (setv apt (and 
    (is-not None (shutil.which "apt-cache"))
    (is-not None (shutil.which "apt-get"))))
  (setv dpkg (is-not None (shutil.which "dpkg")))
  (and apt dpkg))

(defn apt-cache []
  "Speed up apt processing with python-apt"
  (setv args ["--no-cache-dir"])
  (try
    (import apt)
    (except [e ImportError] 
      ((if pip-supported
        (pip-install-import "python-apt" :import-name "apt" :args args))))
    (else
      (def cache (apt.Cache)))
    (finally (if pip-supported
      (pip-install-import "python-apt" :import-name "apt")
      (def cache (apt.Cache))))))

(defn apt-install [package &optional [root False] [args []]]
  "Install a package using pip"
  (setv install (+ ["install"] args [package]))
  (apt-get install))

(defn apt-update [&optional [root False]]
  "Install a package using pip"
  (apt-get "update"))

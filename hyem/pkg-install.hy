#!/usr/bin/hy env
;;;; pkg-install.hy --- Install packages with pip from the PyPI.

(import yaml)
(import [glob [glob]])

(import sh)
(import progressbar)
(import [sh [apt-get]])
(import [getpass [getpass]])

(import [hyem.utils.hyaml [yaml-dict-list]])
(import [hyem.packagers.recipes [pip-packages apt-packages]])
(import [hyem.packagers.hypip [pip-install]])
(import [hyem.packagers.hyapt [apt-install apt-update]])

(def base-recipes-path "recipes/*/*.yml")

(defn glob-recipes [path]
  "Get a list of recipe paths"
  (yaml-dict-list (glob path)))

(defn pypi-install [dictionary-list]
  "Install all pip recipes"
  (setv packages (pip-packages dictionary-list))
  (with [progress (progressbar.ProgressBar :max-value (len packages))]
    (for (package packages)
         (pip-install package :args ["-q"]) (+= progress 1))))

(defn dpkg-install [dictionary-list]
  "Install all dpkg recipes"
  (setv packages (apt-packages dictionary-list))
  (with [progress (progressbar.ProgressBar :max-value (len packages))]
    (for (package packages)
         (apt-install package :args ["-y"]) (+= progress 1))))

;; Master Install
(defn install []
  (setv recipes (glob-recipes base-recipes-path))
  (pypi-install recipes)
  (with [sudo (sh.contrib.sudo (getpass "Password: ") :_with True)]
    (apt-update)
    (dpkg-install recipes)))

;; If __name__ == '__main__':
(defmain [&rest args]
  (install))

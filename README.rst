=====
.hyem
=====

| Noun; yem; hem; hjem; home; heim; 
| place of family, sanctuary, and comfort;
| adverb: "I'm gannin hyem" - I'm going home.
| [Scandinavian/Old English]


Description
===========

Configure the system beyond dotfiles. Currently only supports Debian Linux.


Installation
============

Before running the script setup ``sudo`` by first installing the command.

.. code-block:: bash

    apt-get install sudo

Then adding the following to the ``/etc/sudoers`` file via ``visudo``.

    <username> ALL=PASSWD

Optionally add the following to require specific applications without password. The list includes all programs that require privileges to complete the install.
    
    <username> ALL=NOPASSWD: /usr/bin/make, /usr/bin/apt-get, /usr/sbin/update-alternatives

#!/bin/bash

main_mono_repo='deb http://download.mono-project.com/repo/debian wheezy main'
secondary_mono_repo='deb http://download.mono-project.com/repo/debian wheezy-libjpeg62-compat main'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

touch /etc/apt/sources.list.d/mono-xamarin.list

if ! grep -q -F "$main_mono_repo" /etc/apt/sources.list.d/mono-xamarin.list; then
    sudo echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
fi

if ! grep -q -F "$secondary_mono_reop" /etc/apt/sources.list.d/mono-xamarin.list; then
    sudo echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list
fi

sudo apt-get update
sudo apt-get install build-essential cmake
sudo apt-get install python-dev python3-dev
sudo apt-get install libstd-rust-dev binutils rust-gdb rust-ll
sudo apt-get install libssh2-1
sudo apt-get install npm golang mono-xbuild nodejs node-typescript
sudo apt-get install libmono-microsoft-build-tasks-v4.0-4.0-cil
sudo apt-get install mono-dmcs
curl https://sh.rustup.rs -sSf | sh
cd ~/.vim/plugged/YouCompleteMe
./install.py --clang-completer --system-libclang --system-boost --all

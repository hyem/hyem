#!/bin/bash

sudo apt-get install neovim lua-nvim lua-nvim-dev ruby-neovim neovim-runtime
echo ''
echo ''
sudo pip2 install --upgrade neovim
sudo pip3 install --upgrade neovim
sudo gem install neovim
echo ''
echo ''
sudo mkdir -p "$HOME".config/nvim
touch "$HOME".config/nvim/init.vim
echo ''
echo ''
sudo ln --symbolic --force --directory "$HOME".vim/ "$HOME"/.config/nvim/
sudo ln --symbolic --force "$HOME".vimrc "$HOME"/.config/nvim/init.vim

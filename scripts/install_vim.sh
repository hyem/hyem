#!/bin/bash

read -p "Install latest vim from source? (Yy/Nn)" -n 1 -r
if [[ $REPLY =~ ^[Nn]$ ]]; then
    echo ''
    exit 0
fi


# Package Management {{{
sudo apt-get install libncurses5-dev \
                     libgnome2-dev \
                     libgnomeui-dev \
                     libgtk2.0-dev \
                     libatk1.0-dev \
                     libbonoboui2-dev \
                     libcairo2-dev \
                     libx11-dev \
                     libxpm-dev \
                     libxt-dev \
                     python-dev \
                     python3-dev \
                     ruby-dev \
                     lua5.1 \
                     libtolua-dev \
                     libtolua++5.1-dev \
                     liblua5.1-dev \
                     luajit \
                     libluajit-5.1 \
                     libluajit-5.1-dev \
                     libperl-dev \
                     libcairo2-dev

echo "Installation will not work if vim is already installed."
read -p "Remove vim from the system? (Yy/Nn)" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
    sudo apt-get remove vim \
                        vim-runtime \
                        gvim
                        vim-tiny \
                        vim-common \
                        vim-gui-common
fi
# End Package Management }}}
# Compile vim from source {{{
git clone https://github.com/vim/vim.git
cd vim

./configure --with-features=huge \
            --enable-gui=gtk2 \
            --enable-multibyte \
            --enable-largefile \
            --enable-luainterp=yes \
            --with-luajit \
            --enable-pythoninterp=no \
            --enable-python3interp=yes \
            --enable-rubyinterp=yes \
            --enable-perlinterp=yes \
            --enable-cscope

make VIMRUNTIMEDIR=/usr/share/vim/vim80
sudo make install

cd ..
rm -rf vim
# End compile vim }}}
# Update default editor {{{
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim 1
sudo update-alternatives --set editor /usr/bin/vim
sudo update-alternatives --install /usr/bin/vi vi /usr/bin/vim 1
sudo update-alternatives --set vi /usr/bin/vim
# End update default editor }}}

exit 0

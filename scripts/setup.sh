#!/bin/bash
#
# Setup dotfile storage with a Git bare repositry and an alias command
# for interacting with it, as well as ignoring all unwanted files.
#


# Create Git bare repository to track files
git init --bare $HOME/.cfg

function config {
    /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}

# Hide files we are not explicitly tracking
config config --local status.showUntrackedFiles no

if ! grep -q -F "/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME" $HOME/.bashrc; then
    echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.bashrc
    source ~/.bashrc
fi
